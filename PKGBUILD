# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Stefano Capitani <stefano@manjaro.org>
# Contributor: Fabian Bornschein <fabiscafe-at-mailbox-dot-org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>

pkgbase=gnome-control-center
pkgname=(
  gnome-control-center
  gnome-keybindings
)
pkgver=47.4
pkgrel=1
pkgdesc="GNOME's main interface to configure various aspects of the desktop"
url="https://apps.gnome.org/Settings"
license=(GPL-2.0-or-later)
arch=(x86_64)
depends=(
  accountsservice
  bolt
  cairo
  colord-gtk4
  cups-pk-helper
  dconf
  fontconfig
  gcc-libs
  gcr-4
  gdk-pixbuf2
  glib2
  glibc
  gnome-bluetooth-3.0
  gnome-color-manager
  gnome-desktop-4
  gnome-online-accounts
  gnome-settings-daemon
  gnome-shell
  gnutls
  graphene
  gsettings-desktop-schemas
  gsound
  gst-plugins-good
  gtk4
  hicolor-icon-theme
  json-glib
  krb5
  libadwaita
  libcolord
  libcups
  libepoxy
  libgoa
  libgtop
  libgudev
  libibus
  libmalcontent
  libmm-glib
  libnm
  libnma-gtk4
  libpulse
  libpwquality
  libsecret
  libsoup3
  libwacom
  libx11
  libxi
  libxml2
  pango
  polkit
  smbclient
  sound-theme-freedesktop
  tecla
  udisks2
  upower
)
makedepends=(
  docbook-xsl
  git
  glib2-devel
  meson
  modemmanager
)
checkdepends=(
  python-dbusmock
  python-gobject
  xorg-server-xvfb
)
source=(
  "git+https://gitlab.gnome.org/GNOME/gnome-control-center.git?signed#tag=${pkgver/[a-z]/.&}"
  "git+https://gitlab.gnome.org/GNOME/libgnome-volume-control.git"
  software-updates.patch
  maia-accent-color-support.patch
  manjarolinux-text-rounded.svg
  manjarolinux-text-dark-rounded.svg
)
b2sums=('9a7c967798bbaeb02216908f12bccb6855b0dabde69bcb34ca8a1f002aaccddf4b8d967d829d7e1e581470666b52508e76b593e3c00a35ef13d7a3be33c945dc'
        'SKIP'
        'd310f8db0bf9529dad04b4984b634fd0acbd9bdc727e7d547e026918ade6aa3a5a9a88ef2670e32ed79b8d91c5eb49bf53b0c2ef513fe72226143892da0ed363'
        '8fbde4e423a487f704c2f32cb4716c4b14c693efa17b9c31d3874680cc9cff60376528ea7b8ccedbc9c67c9af4e242b68213b206fb62edcda63960208921c9d8'
        'c307b835dedd0cb4454883103239a6ffeaa5b49cb94b3b7c3de1ffe94369324ffcb9eb5af0b93b6b475181d483c3374903a684c6f1b2c481e4fefa389d4c5c4d'
        '6d3c1bb506c255e3a575be1ee42131d5ac3efaf00a1b9411f0aa7f8183d80dafeac599c5d217931fe85e279226e7fcd28fdc22045cb35c00ed2b0a5689ee7eb0')
validpgpkeys=(
  9B60FE7947F0A3C58136817F2C2A218742E016BE # Felipe Borges (GNOME) <felipeborges@gnome.org>
)

prepare() {
  cd $pkgbase
  git submodule init subprojects/gvc
  git submodule set-url subprojects/gvc "$srcdir/libgnome-volume-control"
  git -c protocol.file.allow=always -c protocol.allow=never submodule update

  # Use Pamac instead of GNOME Software for software updates if installed
  patch -Np1 -i ../software-updates.patch

  # Add Maia accent color support
  patch -Np1 -i ../maia-accent-color-support.patch
}

build() {
  local meson_options=(
    -D documentation=true
    -D location-services=enabled
    -D malcontent=true
    -D distributor_logo=/usr/share/icons/manjaro/manjarolinux-text-rounded.svg
    -D dark_mode_distributor_logo=/usr/share/icons/manjaro/manjarolinux-text-dark-rounded.svg
  )

  arch-meson $pkgbase build "${meson_options[@]}"
  meson compile -C build
}

check() {
  GTK_A11Y=none dbus-run-session xvfb-run -s '-nolisten local +iglx -noreset' \
    meson test -C build --print-errorlogs
}

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

package_gnome-control-center() {
  depends+=(gnome-keybindings)
  optdepends=(
    'fwupd: device security panel'
    'gnome-remote-desktop: screen sharing'
    'gnome-user-share: WebDAV file sharing'
    'malcontent: application permission control'
    'networkmanager: network settings'
    'openssh: remote login'
    'power-profiles-daemon: power profiles'
    'rygel: media sharing'
    'system-config-printer: printer settings'
  )
  groups=(gnome)

  meson install -C build --destdir "$pkgdir"

  install -Dm644 "$srcdir"/manjarolinux{-text,-text-dark}-rounded.svg -t \
    "$pkgdir/usr/share/icons/manjaro/"

  cd "$pkgdir"
  _pick gkb usr/share/gettext/its/gnome-keybindings.*
  _pick gkb usr/share/gnome-control-center/keybindings
  _pick gkb usr/share/pkgconfig/gnome-keybindings.pc
}

package_gnome-keybindings() {
  pkgdesc="Keybindings configuration for GNOME applications"
  depends=()

  mv gkb/* "$pkgdir"
}

# vim:set sw=2 sts=-1 et:

